# Sarah Maas
# CS 325 Algorithms
# sort algorithm timing for homework 1

import insertsort
import mergesort
import random
import timeit

arraySizes = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
#arraySizes = [5, 10]
for n in arraySizes:
    tempArray = []
    for x in range(n):
        tempArray.append(random.randint(0, 10000))
    
    startTime = timeit.default_timer()
    mergesort.mergeSort(tempArray)
    stopTime = timeit.default_timer()
    elapsedTime = stopTime - startTime
    print 'mergeSort time; ' + str(elapsedTime) +'; arraySize; ' + str(n)
    startTime = timeit.default_timer()
    insertsort.insertionSort(tempArray)
    stopTime = timeit.default_timer()
    elapsedTime = stopTime - startTime
    print 'insertionSort time; ' + str(elapsedTime) +'; arraySize; ' + str(n)