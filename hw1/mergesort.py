# Sarah Maas
# CS 325 Algorithms
# merge sort algorithm for homework 1

def mergeSort(array):
    #base case for recursion is length of 1 (or 0)
    if len(array) < 2:
        return array
    #split into two halves and call merge help function
    middle = (len(array) + len(array)%2) / 2;
    left = mergeSort(array[:middle])
    right = mergeSort(array[middle:])
    #print left
    #print right
    return merge(left, right)
    

def merge(left, right):
    leftLength = len(left)
    rightLength = len(right)
    #if a side is zero return the other side
    if leftLength == 0:
        return right
    if rightLength == 0:
        return left
    #compare the terms from left to right 
    merged = []
    leftPosition = 0
    rightPosition = 0
    while len(merged) < leftLength + rightLength:
        if left[leftPosition] < right[rightPosition]:
            merged.append(left[leftPosition])
            leftPosition+=1
        else:
            merged.append(right[rightPosition])
            rightPosition+=1
        #handle the oddball term / reaching the end
        if leftPosition == leftLength:
            #add to left array
            left.append([])
        if rightPosition == rightLength:
            #add to right array
            right.append([])
    return merged
        
#for each row in data.txt
#read in line
#discard first integer
#append following integers to list
#call merge sort on list
#write sorted numbers to merge.out 

inputFilename = 'data.txt'
outputFilename = 'merge.out'
with open(inputFilename) as inputFile, open(outputFilename, 'w+') as outputFile:
    for line in inputFile:
        array = []
        for element in line.split(' '):
            array.append(int(element))
        #print array
        sortedArray = mergeSort(array[1:])
        arrayString = ''
        for num in sortedArray: 
            outputFile.write(str(num) + ' ')
        outputFile.write('\n')
