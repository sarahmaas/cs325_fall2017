# Sarah Maas
# CS 325 Algorithms
# insertion sort algorithm for homework 1

	    
def insertionSort(array):
    for count in range(0, len(array) - 1):
    	for idx in range(0, len(array)-1):
    	   if array[idx] > array[idx+1]:
    	        tempNum = array[idx]
    	        array[idx] = array[idx+1]
    	        array[idx+1] = tempNum

#for each row in data.txt
#read in line
#discard first integer
#append following integers to list
#call insertion sort on list
#write sorted numbers to insert.out

inputFilename = 'data.txt'
outputFilename = 'insert.out'
with open(inputFilename) as inputFile, open(outputFilename, 'w+') as outputFile:
    for line in inputFile:
        array = []
        for element in line.split(' '):
            array.append(int(element))
        #print array
        sortedArray = array[1:]
        insertionSort(sortedArray)
        arrayString = ''
        for num in sortedArray: 
            outputFile.write(str(num) + ' ')
        outputFile.write('\n')



        