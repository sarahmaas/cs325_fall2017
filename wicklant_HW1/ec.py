# Ec.py
# Author: Tira Palfini
# Date: 09/30/17
# This program generates arrays of specified type and size (specified by the bash script). 
# It then will take either the insertion sort or mergesort algorithm and time how long 
# It takes to run the algorithm.  The process of generating and sorting the array is performed
# 5 times and the average time is then written.  


import insertsort
import mergesort
import random
import timeit
import sys

arraySize = int(sys.argv[1])

avgtime = 0
sort = str(sys.argv[2])

arrtype = str(sys.argv[3])

for x in range(5):
	#Citation https://stackoverflow.com/questions/12167863/big-array-with-random-numbers-with-python
	#array = [random.randint(0, 10000) for _ in xrange(arraySize)]
    
    array = []
    i = 2
    j = 1

    if arrtype == "asc":
        for x in range(arraySize):
            array.append(x)

    elif arrtype == "dec":
        i = arraySize
        for x in range(arraySize):
            array.append(i)
            i -= 1
    
    elif arrtype == "rand":
    	array = [random.randint(0, 10000) for _ in xrange(arraySize)]

    elif arrtype == "interwoven":
	    i = 2
	    j = 1
	    for x in range(arraySize/2):
	        array.append(i)
	        i += 2

	    for x in range(arraySize/2):
	    	array.append(j)
	    	j += 2
    #print array

    start_time = timeit.default_timer()
    if sort == "insert":
	    insert_array = insertsort.insertsort(array)
    elif sort == "merge":
        merge_sort = mergesort.mergesort(array)
    total_time = timeit.default_timer() - start_time

	#print("%.4f seconds" %total_time)
    avgtime += total_time

avgtime = avgtime / 5

#print ("Average time for %d elements using %s sort was %.4f seconds" %(arraySize, sort, avgtime)) 
sys.stdout.write("%d, %.4f\r" %(arraySize, avgtime))