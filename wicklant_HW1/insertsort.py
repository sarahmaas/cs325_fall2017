# InsertSort is an algorithm to sort an unsorted array increasing in size coded in Python
# Author: Tira Palfini
# Date: 09/23/17
# Comments: Pseudocode from Harvard CS50 lecture: https://www.youtube.com/watch?v=DFG-XuyPYUQ 


def insertsort(array):

	#Iterate throguh array
    for i in xrange(1, len(array)):
        element = array[i]
        index = i
        # Iterate through sorted array to place each element in proper order
        while ( index > 0 and array[index -1] > element):
            array[index] = array[index-1]
            index = index -1
            array[index] = element
    return array



