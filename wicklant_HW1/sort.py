#Sort.py
# Author: Tira Palfini
# Date: 09/28/17
# Sort.py is driver program that will run mergesort and insertion sort.  It reads input from the
# filed named 'data.txt' and formats each line to an array.  The array is then passed to both
# mergesort and insertsort which will sort the array and write the sorted array to the 
# appropriate output files (insert.out or merge.out). 


import insertsort
import mergesort

# Open input file and output files
with open('data.txt', 'r') as in_file, open('insert.out', 'w+') as insert_file, open('merge.out', 'a') as merge_file:

	for line in in_file:
		# Take each line and add elements to array
	    array = []
	    for element in line.split(' '):
		    array.append(int(element))
	    array.remove(array[0])

	    # Perform Insertion Sort on array
	    insert_array = insertsort.insertsort(array)

	    # Make array elements into string to write in output file 'insert.out'
	    output = ""
	    for number in insert_array:
             output += str(number) + " "
	    insert_file.write(output + "\n")

	    # Perform Mergesort on original array
	    merge_array = mergesort.mergesort(array)

	    # Make each array elements into string to write to output file 'merge.out'
	    output = ""
	    for number in merge_array:
	    	output += str(number) + " "
	    merge_file.write(output + "\n")
        
in_file.close()
insert_file.close()
merge_file.close()
