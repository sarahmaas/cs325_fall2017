# MergeSort is an algorithm to sort an unsorted array increasing in size coded in Python
# Author: Tira Palfini
# Date: 09/27/17
# Citations: https://pythonandr.com/2015/07/05/the-merge-sort-python-code/
#            http://www.geeksforgeeks.org/iterative-merge-sort/

def merge(left, right):
    
    # Create temporary array to hold sorted elements
    temp = []

    # Iterators
    i = j = 0

    # While elements still left in left and right arrays to be compared
    while i < len(left) and  j < len(right):
    	# Determine smaller element and add to temporary array
        if left[i] < right[j]:
            temp.append(left[i])
            i += 1
        else:
            temp.append(right[j])
            j += 1

    # Add any remaining elements from left array
    while i < len(left):
        temp.append(left[i])
        i += 1 
    
    # Add any remaining elements from right array
    while j < len(right):
        temp.append(right[j])
        j += 1

    return temp

def mergesort(array):

	# Base Case
    if len(array) == 0 or len(array) == 1:
        return array

    # Continue splitting the array in half
    else:
        mid = len(array)//2 
        left = array[:mid]
        right = array[mid:]

        mergesort(left)
        mergesort(right)
        
        # Merge the left and right arrays
        merged = merge(left, right)

        return merged