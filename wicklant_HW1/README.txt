Title: Homework 1 Code
Date: 09/30/17
Author: Tira Palfini
Description: This is the code for Problem 4 and the Extra Credit assignment for Homework 1
The programs for sorting (mergesort and insertsort were implemented with python.  
The extra credit assignment uses a bash script to run the driver program.  
**************************************************

Problem 4
##################################################
The driver program (sort.py) will take input from a file called “data.txt” 
and sort the arrays listed in the text file with both mergesort and insertsort. 
The sorted array will be written to files called “merge.out” and “insert.out” 
(depending on which sorting algorithm was utilized).  
To run the program, type the following into command line:

    python sort.py

The zip file contains a data.txt file that was given to use in discussion
this week. 


Extra Credit
##################################################
The bash script (ec) will pass the various array types, sorting alorithms, and
array sizes to the driver program (ec.py).  The array type and algorithm type will 
be written to the file ec.cvs.  The driver program will then utilize those variables 
listed to create the array (based on type) of specified size. It will then sort the 
array based on the algorithm specified. The time it takes the algorithm to sort the 
array will be calculated. The array will be created and sorted 5 times and the 
array size and average time written to the written to the file ec.csv.  
To run the program, type the following into command line:
    
    bash ec

The cvs file was created for the extra credit to make the data easily imported
in Microsoft Excel for analysis.  
