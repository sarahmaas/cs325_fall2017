# CS 325 Algorithms
# wrestler (bipartite graph) algorithm for homework 5


def enum(**enums):
     return type('Enum', (), enums)

TEAMS = enum(BABYFACE=0, HEEL=1)


# this is related to two color or bipartite graph style
# of algorithm, implemented using BFS and tracking colors
# and using an adjacency list
def makeTeams(wrestlersAdjDict):
    unvisited = wrestlersAdjDict.keys()
    team = dict()
    currentTeam = TEAMS.BABYFACE
    while len(unvisited) > 0:
        # do BFS from the first position
        queue = []
        # mark as visited and push to queue
        # add to team w/color, remove from unvisited list
        team[unvisited[0]] = currentTeam
        queue = [unvisited[0]]
        unvisited.remove(unvisited[0])
        while queue:
            vertex = queue.pop()
            # toggle team
            currentTeam = TEAMS.HEEL if team[vertex] == TEAMS.BABYFACE else TEAMS.BABYFACE
            # iterate through neighbors
            for v in wrestlersAdjDict[vertex]:
                # if it's already been visited and team doesn't match
                if v in team and team[v] != currentTeam:
                    return False
                if v not in team:
                    # mark as visited and push to queue
                    # add to team w/color, remove from unvisited list
                    team[v] = currentTeam
                    unvisited.remove(v)       
                    queue.append(v)
    return team


# read in from file given and build adjancency list
def main():
    from sys import argv
    if len(argv) != 2:
        print 'programName filename.txt'
        return
    inputFilename = argv[1]
    with open(inputFilename) as inputFile:
        count = 0
        for line in inputFile:
            linesToRead = int(line)
            count = count + 1
            wrestlers = dict()
            for w in range(linesToRead):
                nextLine = next(inputFile)
                wrestlers[nextLine.rstrip()] = set()
            #print str(wrestlers)
            count = 0
            nextLine = next(inputFile)
            linesToRead = int(nextLine)
            count = count + 1
            array = []
            for r in range(linesToRead):
                nextLine = next(inputFile)
                # set up rivalry
                rivalry = []
                for element in nextLine.split(' '):
                    rivalry.append(element.rstrip())
                # add relationship both ways
                wrestlers[rivalry[0]].add(rivalry[1])
                wrestlers[rivalry[1]].add(rivalry[0])
                array.append(rivalry)
            # split rivalries into two distinct teams    
            result = makeTeams(wrestlers)
            # print results for distinct teams
            if result == False:
                print 'No'
            else:
                print 'Yes'
                print 'Babyfaces: ' + str(" ".join( str(w) for w in result if result[w] == TEAMS.BABYFACE))
                print 'Heels: ' + str(" ".join( str(w) for w in result if result[w] == TEAMS.HEEL))


main()


