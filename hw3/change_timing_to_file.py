# Sarah Maas
# CS 325 Algorithms
# change algorithm timing for homework 3

import makechange
import random
import timeit

denominations = list(range(1, 50, 2))
amounts = list(range(1000, 5500, 500))

#n=3..10 and A =1000, .. 5000 by 500

outputFilename = 'sort_timing_change.out'
with  open(outputFilename, 'w+') as outputFile:
    outputFile.write('algorithm; time; n; Amount; n*Amount\n')
    for amount in range(1000, 5500, 500):
        for n in range(3, 11):
            tempArray = denominations[0:n]
            startTime = timeit.default_timer()
            makechange.makeChange(tempArray, amount)
            stopTime = timeit.default_timer()
            elapsedTime = stopTime - startTime
            print 'makechange: time ' + str(elapsedTime) +', denominations ' + str(n)+', Amount ' + str(amount)+', n*A ' + str(n*amount)
            outputFile.write('makechange; ' + str(elapsedTime) +'; ' + str(n) +'; ' + str(amount) +'; ' + str(n*amount))
            outputFile.write('\n')
