# Sarah Maas
# CS 325 Algorithms
# making change algorithm for homework 3

def makeChange(coins, amount):
    numCoins = len(coins)
    # initialize the two arrays to sentinel values - inf and -1
    total = [float('inf') for x in range(amount+1)]
    coinUsed = [-1 for x in range(amount + 1)]
    total[0] = 0
    # iterate through the coins and if the number is larger or 
    # equal to the coin value, it could potentially be used.
    idx = 0
    for c in coins:
        for num in range(1, amount + 1):
            if num >= c:
                if total[num - c] + 1 < total[num]:
                    total[num] = total[num - c] + 1
                    coinUsed[num] = idx
        idx = idx + 1
    # find the coins that have been used and add counts to an
    # array to be returned
    idx = len(coinUsed) - 1 
    coinsReturned = [ 0 for x in range(numCoins)]
    while idx != 0:
        coin = coinUsed[idx]
        coinsReturned[coin] = coinsReturned[coin] + 1
        idx = idx - coins[coin]
    return (coinsReturned, total[amount])

def test(): 
    denominations = [1, 3, 7, 12] 
    amount = 29
    answer =  makeChange(denominations, amount)
    print( " ".join( str(i) for i in answer[0] ) )
    print answer[1]


def main():
    inputFilename = 'amount.txt'
    outputFilename = 'change.txt'
    with open(inputFilename) as inputFile, open(outputFilename, 'w+') as outputFile:
        for line in inputFile:
            nextLine = next(inputFile)
            array = []
            for element in line.split(' '):
                array.append(int(element))
            change = makeChange(array, int(nextLine))
            # write denominations, amount, coins required, and amount to file
            outputFile.write( " ".join( str(i) for i in array ) )
            outputFile.write('\n')
            outputFile.write(nextLine)
            outputFile.write( " ".join( str(i) for i in change[0] ) )
            outputFile.write('\n')
            outputFile.write(str(change[1]))
            outputFile.write('\n')

main()
