# Sarah Maas
# CS 325 Algorithms
# sort algorithm timing for homework 1

import insertsort
import mergesort
import stoogesort2
import random
import timeit

arraySizes = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]
#arraySizes = [5, 10]
outputFilename = 'sort_timing_all.out'
with  open(outputFilename, 'w+') as outputFile:
    outputFile.write('sort_type; time; arraySize\n')
    for n in arraySizes:
        tempArray = []
        for x in range(n):
            tempArray.append(random.randint(0, 10000))
        
        startTime = timeit.default_timer()
        stoogesort2.stoogeSort(tempArray)
        stopTime = timeit.default_timer()
        elapsedTime = stopTime - startTime
        print 'stoogesort: time ' + str(elapsedTime) +', arraySize ' + str(n)
        outputFile.write('stoogesort; ' + str(elapsedTime) +'; ' + str(n))
        outputFile.write('\n')
