# Sarah Maas
# CS 325 Algorithms
# sort algorithm timing for homework 1

import insertsort
import mergesort
import random
import timeit

arraySizes = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
#arraySizes = [5, 10]
for n in arraySizes:
    tempArray = []
    for x in range(n):
        tempArray.append(random.randint(0, 10000))
    
    startTime = timeit.default_timer()
    mergesort.mergeSort(tempArray)
    stopTime = timeit.default_timer()
    elapsedTime = stopTime - startTime
    print 'mergeSort time; ' + str(elapsedTime) +'; arraySize; ' + str(n)
    startTime = timeit.default_timer()
    insertsort.insertionSort(tempArray)
    stopTime = timeit.default_timer()
    elapsedTime = stopTime - startTime
    print 'insertionSort time; ' + str(elapsedTime) +'; arraySize; ' + str(n)
    
# Sarah Maas
# CS 325 Algorithms
# sort algorithm timing for homework 1

import insertsort
import mergesort
import stoogesort2
import random
import timeit

arraySizes = [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000]
#arraySizes = [5, 10]
outputFilename = 'sort_timing_old.out'
with  open(outputFilename, 'w+') as outputFile:
    outputFile.write('sort_type; time; arraySize')
    for n in arraySizes:
        tempArray = []
        for x in range(n):
            tempArray.append(random.randint(0, 10000))
        
        startTime = timeit.default_timer()
        insertsort.insertionSort(tempArray)
        stopTime = timeit.default_timer()
        elapsedTime = stopTime - startTime
        print 'insertion sort: time ' + str(elapsedTime) +', arraySize ' + str(n)
        outputFile.write('insertion sort; ' + str(elapsedTime) +'; ' + str(n))
        outputFile.write('\n')
    for n in arraySizes:
        tempArray = []
        for x in range(n):
            tempArray.append(random.randint(0, 10000))
        
        startTime = timeit.default_timer()
        mergesort.mergeSort(tempArray)
        stopTime = timeit.default_timer()
        elapsedTime = stopTime - startTime
        print 'merge sort: time ' + str(elapsedTime) +', arraySize ' + str(n)
        outputFile.write('merge sort; ' + str(elapsedTime) +'; ' + str(n))
        outputFile.write('\n')