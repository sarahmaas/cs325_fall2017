# Sarah Maas
# CS 325 Algorithms
# sort algorithm timing for homework 1

import stoogesort2
import random
import timeit

arraySizes = [1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
#arraySizes = [5, 10]
for n in arraySizes:
    tempArray = []
    for x in range(n):
        tempArray.append(random.randint(0, 10000))
    
    startTime = timeit.default_timer()
    stoogesort2.stoogeSort(tempArray)
    stopTime = timeit.default_timer()
    elapsedTime = stopTime - startTime
    print 'stoogesort time; ' + str(elapsedTime) +'; arraySize; ' + str(n)
