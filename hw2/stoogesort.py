# Sarah Maas
# CS 325 Algorithms
# stooge sort algorithm for homework 2

import numpy
import math

def stoogeSort(arrayIn):
    newArray = numpy.array(arrayIn)
    stoogeSortHelper(newArray)
    for idx in range(len(newArray)):
        arrayIn[idx] = newArray[idx]

#StoogeSort(A[0 ... n - 1])
    #if n = 2 and A[0] > A[1]
        #swap A[0] and A[1]
    #else if n > 2
        #m = ceiling(2n/3)
        #StoogeSort(A[0 ... m - 1])
        #StoogeSort(A[n - m ... n - 1])
        #Stoogesort(A[0 ... m - 1]) 

def stoogeSortHelper(array):
    n = len(array)
    if n == 2 and array[0] > array[1]:
        array[0], array[1] = array[1], array[0]
    elif n > 2:
        #m = int(2*n // 3)
        m = int(math.ceil(2*n / 3.0))
        stoogeSortHelper(array[0:m])
        stoogeSortHelper(array[n-m:n])
        stoogeSortHelper(array[0:m])

#for each row in data.txt
#read in line
#discard first integer
#append following integers to list
#call stooge sort on list
#write sorted numbers to stooge.out

inputFilename = 'data.txt'
outputFilename = 'stooge.out'
with open(inputFilename) as inputFile, open(outputFilename, 'w+') as outputFile:
    for line in inputFile:
        array = []
        for element in line.split(' '):
            array.append(int(element))
        #print array
        sortedArray = array[1:]
        stoogeSort(sortedArray)
        arrayString = ''
        for num in sortedArray: 
            outputFile.write(str(num) + ' ')
        outputFile.write('\n')

        