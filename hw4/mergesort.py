# Sarah Maas
# CS 325 Algorithms
# merge sort algorithm for homework 1

def mergeSort(array):
    #base case for recursion is length of 1 (or 0)
    if len(array) < 2:
        return array
    #split into two halves and call merge help function
    middle = int((len(array) + len(array)%2) / 2);
    left = mergeSort(array[:middle])
    right = mergeSort(array[middle:])
    #print left
    #print right
    return merge(left, right)
    

def merge(left, right):
    leftLength = len(left)
    rightLength = len(right)
    #if a side is zero return the other side
    if leftLength == 0:
        return right
    if rightLength == 0:
        return left
    #compare the terms from left to right 
    merged = []
    leftPosition = 0
    rightPosition = 0
    while len(merged) < leftLength + rightLength:
        print left[leftPosition], right[rightPosition]
        if left[leftPosition][1] < right[rightPosition][1]:
            merged.append(left[leftPosition])
            leftPosition+=1
        else:
            merged.append(right[rightPosition])
            rightPosition+=1
        #handle the oddball term / reaching the end
        if leftPosition == leftLength:
            #add to left array
            left.append([])
        if rightPosition == rightLength:
            #add to right array
            right.append([])
    return merged
        
