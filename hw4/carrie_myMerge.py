import mergesort
import abettersort

class Activity:
    def __init__(self, act_id, start, end):
        self.act_id = act_id
        self.start = start
        self.end = end
 
#create objects
activity1 = Activity(1, 9, 11)
activity2 = Activity(2, 8, 10)
activity3 = Activity(3, 6, 8)
activity4 = Activity(4, 4, 5)
activity5 = Activity(5, 1, 3)
print type(activity5) 
array = []
array.append(activity1)
array.append(activity2)
array.append(activity3)
array.append(activity4)
array.append(activity5)
 
print array 
def last_to_start(acts):
    schedule = []
 
    #sort values first
    abettersort.mergesort(acts)
 
    #once values are sorted, grab first value
    schedule.append(acts[0].act_id)
   
    #go through remaining items and schedule
    for i in range(0, len(acts) - 1):
        if acts[i + 1].end <= acts[i].start:
            schedule.append(acts[i+1].act_id)
 
    return (schedule, len(schedule))   
 
#mergesort function
def mergesortC(array, size):
        if size <= 1:  #if the size of the array is less than or equal to 1, then we don't need to sort anything
                return array
 
        #split the array size in 2  
        middle = len(array)/2
        #put one half of the array into each of the two subarrays        
        array1 = array[:middle]
        array1 = mergesortC(array1, len(array1)) #recursively call mergesort on half of array
        array2 = array[middle:]
        array2 = mergesortC(array2, len(array2)) #recursively call mergesort on half of array
 
        return(merge(array1, array2)) #return the result of the merged subarrays        
 
#merge functions, which merges sorted subarrays in correct sorted order
def merge(array1, array2):
        mergedarray = []
        #initialize values
        item1 = 0
        item2 = 0
        #continue looping until the size of both arrays is not greater than the size of the new merged array
        while ((len(array1)+len(array2)) > len(mergedarray)):
                #if array1 is smaller array
                if array1[item1].start >  array2[item2].start:
                        mergedarray.append(array1[item1].start) #add value at item1 to new array
                        item1 = item1 + 1 #increment to next subscript
                else:
                        mergedarray.append(array2[item2].start) #add value at item2 to new array
                        item2 = item2 + 1 #increment to next subscript
 
                #shortcut - add entire array to merged array if there is 1 array remaining
                if item1 == len(array1) or item2 == len(array2):
                        mergedarray.extend(array1[item1:] or array2[item2:])
                        break
 
 
        return mergedarray # return the merged and sorted array
 
(schedule, number) = last_to_start(array)
print schedule
print number 
 
inputFilename = 'act.txt'
#inputFilename = 'test.txt'
with open(inputFilename) as inputFile:
    for line in inputFile:
        linesToRead = int(line)
        print 'linesToRead: ' + str(linesToRead)
        array = []
        for act in range(linesToRead):
            #print act
            nextLine = next(inputFile)
            arrayLine = []
            for element in nextLine.split(' '):
               arrayLine.append(int(element))
            newAct = Activity(arrayLine[0], arrayLine[1], arrayLine[2])
            array.append(newAct)
        print array
 
 
        (schedule, number) = last_to_start(array)
        print schedule
        print number