# Sarah Maas
# CS 325 Algorithms
# last to start algorithm for homework 4


def lastToStart(listOfActivities):
    act_id = 0
    start = 1
    end = 2
    schedule = []
    
    # sort by start time
    listOfActivities = mergesort(listOfActivities)

    # select last activity to start
    schedule.insert(0, listOfActivities[0][act_id])
    idxUsed = 0
    # find next activity that ends before 
    for idx in range(1, len(listOfActivities)):
        if listOfActivities[idx][end] <= listOfActivities[idxUsed][start]:
            schedule.insert(0,listOfActivities[idx][act_id])
            idxUsed = idx
 
    return (schedule, len(schedule))     
    
# read in from file 'act.txt'. Stray space was removed from 
# the txt file I uploaded here vs the one provided on Canvas
 
def main():
    inputFilename = 'act.txt'
    with open(inputFilename) as inputFile:
        count = 0
        for line in inputFile:
            linesToRead = int(line)
            count = count + 1
            array = []
            for act in range(linesToRead):
                #print act
                nextLine = next(inputFile)
                arrayLine = []
                for element in nextLine.split(' '):
                    arrayLine.append(int(element))
                array.append(arrayLine)

            # make schedule and print schedule and # of activities
            (schedule, number) = lastToStart(array)
            print "Set " + str(count)
            print 'Number of activities selected = ' + str(number)
            print 'Activities: ' + str(" ".join( str(i) for i in schedule ))


# My mergesort was NOT set up to work with lists in a way that
# worked with this problem and this was a problem for a number 
# of others as well, so we found this implementation that works
# well with a list of lists and it was shared fairly widely on
# slack and the HW group discussion board since we were not to
# use built in sort.

# https://pythonandr.com/2015/07/05/the-merge-sort-python-code/

# Code for the merge subroutine

def merge(a,b):
    """ Function to merge two arrays """
    c = []
    while len(a) != 0 and len(b) != 0:
        if a[0][1] > b[0][1]:
            c.append(a[0])
            a.remove(a[0])
        else:
            c.append(b[0])
            b.remove(b[0])
    if len(a) == 0:
        c += b
    else:
        c += a
    return c

# Code for merge sort

def mergesort(x):
    """ Function to sort an array using merge sort algorithm """
    if len(x) == 0 or len(x) == 1:
        return x
    else:
        middle = len(x)/2
        a = mergesort(x[:middle])
        b = mergesort(x[middle:])
        return merge(a,b)
        
def prettyPrintList(listLists):
    for line in listLists:
        print line


main()
 
 
