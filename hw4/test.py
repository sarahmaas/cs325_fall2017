def ex():
    inputFilename = 'amount.txt'
    outputFilename = 'change.txt'
    with open(inputFilename) as inputFile, open(outputFilename, 'w+') as outputFile:
        for line in inputFile:
            nextLine = next(inputFile)
            array = []
            for element in line.split(' '):
                array.append(int(element))
            change = makeChange(array, int(nextLine))
            # write denominations, amount, coins required, and amount to file
            outputFile.write( " ".join( str(i) for i in array ) )
            outputFile.write('\n')
            outputFile.write(nextLine)
            outputFile.write( " ".join( str(i) for i in change[0] ) )
            outputFile.write('\n')
            outputFile.write(str(change[1]))
            outputFile.write('\n')


def main():
    inputFilename = 'act.txt'
    with open(inputFilename) as inputFile:
        for line in inputFile:
            linesToRead = int(line)
            print 'linesToRead: ' + str(linesToRead)
            array = []
            for act in range(linesToRead):
                #print act
                nextLine = next(inputFile)
                arrayLine = []
                for element in nextLine.split(' '):
                    arrayLine.append(int(element))
                #print arrayLine
                array.append(arrayLine)
            print array                        
    
main()